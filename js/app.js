
// ==============================
// THE MAIN APPLICAIOTN LOGIC .
// ==============================

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                   = require('jquery'),
        Backbone            = require('backbone'),
        _                   = require('underscore'),
        Fastclick           = require('fastclick'),
        Router              = require('router'),
        bootstrap           = require('bootstrap');
        

    var initialize = function(){
  

    var appRouter = new Router();  //define our new instance of router   
    Backbone.history.start();   // use # History API
    //Backbone.history.start({pushState: true, root: "/work/2014/Bravand/LovedByKids/lbk-backboned/"});   // use html5 History API
    
    //ADD FASTCLICK
    FastClick.attach(document.body);
   

    console.log("its up")

    //LISTEN TO SEE IF THIS IS HOST PLAYER
    // PlayersCollection.models[0].on('change:monitor', function(model, updatedQuantity) {
    //         appRouter.navigate('monitor_ss', {trigger:true }); // TRIGGERS THE PAGE FROM ROUTER 
    // });

    
    // document.ontouchmove = function(e){
    //          e.preventDefault();
    // }

  }


  return {
    initialize: initialize

  };

});
