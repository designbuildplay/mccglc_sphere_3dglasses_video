define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $           = require('jquery'),
        Backbone    = require('backbone'),
        _           = require('underscore'),
        Player     = require('models.Player');

    // CONTENT :::::::::::::::::::::::::::::::::::

    var PlayerCollection = Backbone.Collection.extend({
      model:Player,
      url:"/"
    });

    var Players = new PlayerCollection([
      { 
        id: 1 
        },
    ]); 


    // Return the model for the module
    return PlayerCollection, Players;


});