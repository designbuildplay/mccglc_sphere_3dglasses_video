// THE INTRO VIEW ::::::::::::::::::::::::

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES ::::::::::::::::::::::::
    var $                  = require('jquery'),
        Backbone           = require('backbone'),
        _                  = require('underscore'),
        TweenMax           = require('tweenmax'),
        videojs              = require('video'),
        projectTemplate    = require("text!templates/home.html"); 

    // CONTENT :::::::::::::::::::::::::::::::::::

    var scope;

    var IntroView = Backbone.View.extend({

        tagName:'div',
        id:"slides",    
        el:'#viewport',  //selects element rendering to
        template: _.template( projectTemplate ), //selects the template with given name
        
        events: {
             // 'click #btn1': 'answer',
        },


        initialize:function (appsocket) {
              scope = this;
              this.render();

              this.videoForward = videojs('sphere3d');
              this.videoForward.controls( false );

              console.log("VIEWING INTRO")
        },
      
        render:function () {
            this.$el.html(this.template());

        
            setTimeout(function(){
                 scope.videoForward.play();
            },10);

            return this;
        },

        // answer:function(ev){
        //    var ans = $(ev.target).data('answer');
        //    console.log("you picked ",ans)
        // },

        // Clean hanging events of the view on change :::::::::::::::::::
        dispose:function(){
            console.log('cleaned')
        }

    });


    // Our module now returns our view
    return IntroView;

});
