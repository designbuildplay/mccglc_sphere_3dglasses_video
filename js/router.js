
// ========
// ROUTER : 
// ========

define(function (require) {

    "use strict";

    // DEFINE THE REQUIRES :::::::::::::::::::

    var $              = require('jquery'),
        Backbone       = require('backbone'),
        _              = require('underscore'),
        OuterView      = require('app/views/outer_ui'),
        Intro          = require('view.intro');


    var scope;
    var mainView = null;
    var subView = null;
    var container = '#container';
    var inner = '#inner'
    var el = "<div id='viewport'></div>";

   //define router class
    var AppRouter = Backbone.Router.extend ({
        routes: {
            '' : 'container',
            'intro' : 'intro',
            'active' : 'active',
            'cabinet' : 'cabinet',
            'shower' : 'shower',
            'door' : 'door',
        },
        
        initialize: function(){
               scope = this;
        },

        newView: function(view){

            if(mainView){
                mainView.unbind()
                mainView.remove()
            }
            
            mainView = new view;
        },

        subView: function(view){

            if(subView){
                subView.dispose()
                subView.unbind()
                subView.remove()
                $(inner).append(el)
            }
            
            if(!mainView){
                scope.newView( OuterView ); //Creates the main view if not there already
            }
            
            console.log('view is', subView)
            subView = new view;
        },

        container: function () {
            //scope.newView( OuterView );
            scope.subView( Intro );
        },

        intro: function () {
            //Pass in view to cleaner func
            scope.subView( Intro );
        },

        active: function () {
            //Pass in view to cleaner func
            scope.subView( Intro );
        },
        
        cabinet: function () {
            //Pass in view to cleaner func
            scope.subView( Intro );
        },

        door: function () {
            //Pass in view to cleaner func
            scope.subView( Intro );
        },

        shower: function () {
            //Pass in view to cleaner func
            scope.subView( Intro );
        },
 

    });


  return AppRouter

});
